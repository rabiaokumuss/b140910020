﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using yemektarifi.Models;
using System.Globalization;
using System.IO;

namespace yemektarifi.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        [HttpPost]
        public ActionResult Index()
        {
            tarifEntities db = new tarifEntities();
            yimek ye = new yimek();
            ye.mama = db.yemekler.ToList();
            return View(ye);

        }
        public ActionResult giris()
        {

            return View();
        }
        [HttpPost]
        public ActionResult giris(kullanicilar Model)
        {
            tarifEntities db = new tarifEntities();
            var kullanici = db.kullanicilar.FirstOrDefault(x=>x.mail == Model.mail && x.sifre ==Model.sifre );
            if(kullanici != null)
            {
                Session["email"] = kullanici;
                return RedirectToAction("Index","Home");
            }
            return View();
        }
        public ActionResult kaydol()
        {

            return View();
        }
        public ActionResult cikis()
        {
            Session.Clear();
            return View();
        }
        [HttpPost]
        public ActionResult kaydol(FormCollection form)
        {
            tarifEntities db = new tarifEntities();
            kullanicilar kul = new kullanicilar();
            kul.adi = form["adi"];
            kul.mail = form["mail"];
            kul.sifre = form["sifre"];
            kul.tel = form["tel"];
            db.kullanicilar.Add(kul);
            db.SaveChanges();
            return View();
        }
        public ActionResult YemekTarifleri()
        {
            return View();
        }
        public ActionResult AnaYemekler()
            {

            return View();

            }
        public ActionResult SebzeYemekleri()
        {
            return View();
        }
        public ActionResult ettavukyemekleri()
        {
            return View();
        }
        public ActionResult Zeytinyaglilar()
        {
            return View();
        }
        public ActionResult PilavTarifleri()
        {
            return View();
        }
        public ActionResult TatliTarifleri()
        {
            return View();
        }
        public ActionResult SutluTatlilar()
        {
            return View();
        }
        public ActionResult SerbetliTatlilar()
        {
            return View();
        }
        public ActionResult tarifgonder()
        {
            return View();
        }

        [HttpPost]
        public ActionResult tarifgonder(FormCollection form, HttpPostedFileBase uploadfile)
        {
            tarifEntities db = new tarifEntities();
            yemekler yem = new yemekler();
            yem.adi = form["adi"].Trim();
            yem.icerik = form["icerik"].Trim();
            yem.url = "/images/" + uploadfile.FileName;
            db.yemekler.Add(yem);
            if(uploadfile.ContentLength>0)
            {
                string filePath = Path.Combine(Server.MapPath("~/images"), Guid.NewGuid().ToString() + Path.GetFileName(uploadfile.FileName));
                uploadfile.SaveAs(filePath);
            }
            db.SaveChanges();
            return View();
        }
        public ActionResult ChangeCulture(string lang, string returnUrl)
        {
            Session["Culture"] = new CultureInfo(lang);
            return Redirect(returnUrl);
        }
        public ActionResult yemektarifi()
        {
            tarifEntities db = new tarifEntities();
            yimek ye = new yimek();
            ye.mama = db.yemekler.ToList();
            var makaleid = Request.QueryString["id"];
            Session["aciklama"] = makaleid;

            return View(ye);
        }

        public class yimek
        {
            public List<yemekler> mama { get; set; }
        }
    }
}
    
