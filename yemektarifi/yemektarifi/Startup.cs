﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(yemektarifi.Startup))]
namespace yemektarifi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
